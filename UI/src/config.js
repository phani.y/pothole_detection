/*
 *  Copyright 2020 Huawei Technologies Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

var host = '10.15.103.48'
// var host = window.location.host
// var port = window.location.port
var port1 = 30210
// var port1 = 5000
var port2 = 30211
export default {

  baseUrl: 'https://' + host + ':' + port1,

  baseUrl_NodeProxy: 'https://' + host + ':' + port2
}
