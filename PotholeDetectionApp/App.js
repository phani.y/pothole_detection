import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

// ...
class App extends Component {
  render() {
    return <WebView source={{ uri: 'http://192.168.185.42:8080/#/' }} />;
  }
}

export default App;