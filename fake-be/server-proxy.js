
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios')
const https = require('https');
const fs = require('fs');
const app = express();
app.use(cors());
const path = require('path');
const port = 3000;
let latLong = {};
var multer  = require('multer')
let BE_URL = 'https://10.15.103.48:30210'

const privateKey = fs.readFileSync(path.join(__dirname, 'certs', 'tls.key'));
const certificate = fs.readFileSync(path.join(__dirname, 'certs', 'tls.crt'));
const options = {
   key: privateKey,
   cert: certificate
};

app.use('/static', express.static(path.join(__dirname, './public')));
app.use(bodyParser.json());

axios.defaults.httpsAgent = new https.Agent({
  rejectUnauthorized: false,
  ca: [
    fs.readFileSync(path.join(__dirname, 'certs', 'ca.crt'))
  ]
})

app.get('/', function(req, res) {
  res.send('connected to server successfully');
})

app.get('/pothole_data', (req,res) => {
  axios.get(BE_URL + '/pothole_data', req.headers)
  .then(response => {
    console.log('potholedata' , response.data)
    return res.json(response.data);
  })
  .catch(error => {
    console.log('potholedata' , error)
    console.error(error)
    return res.send(error);
  })
})

/****usersList***/

app.get('/users_data', (req,res) => {
  axios.get(BE_URL + '/users_data', req.headers)
  .then(response => {
    console.log('usersdata' , response.data)
    return res.json(response.data);
  })
  .catch(error => {
    console.log('usersdataerr' , error)
    console.error(error)
    return res.send(error);
  })
})


app.get('/location_map', (req,res) => {
  axios.get(BE_URL + '/location_map', req.headers)
  .then(response => {
    console.log('locationmapdata', response.data)
    return res.send(response.data);
  })
  .catch(error => {
    console.log('locationmaperror', error)
    console.error(error)
    return res.send(error);
  })
})
app.get('/update_location', (req,res) => {
  axios.get(BE_URL + '/update_location', req.headers)
  .then(response => {
    console.log('updateLocation', response.data)
    return res.json(response.data);
  })
  .catch(error => {
    console.log('updateLocationerr', error)
    console.error(error)
    return res.send(error);
  })
})

app.post('/device_ip', (req,res) => {
  console.log('deviceIPreq', req.body);
  axios.post(BE_URL + '/device_ip',req.body, req.headers)
  .then(response => {
    console.log('deviceIPdata', response.data)
    return res.json(response.data);
  })
  .catch(error => {
    console.log('deviceIPerr', error)
    console.error(error)
    return res.send(error);
  })
})

/****login***/

app.post('/Login', (req,res) => {
  console.log('logindata..', req.body);
  axios.post(BE_URL + '/Login',req.body, req.headers)
  .then(response => {
    console.log('login...', response.data)
    return res.json(response.data);
  })
  .catch(error => {
    console.log('loginerr:', error)
    console.error(error)
    return res.send(error);
  })
})


app.get('/loadhtml', (req,res) => {
  let filePath = path.join(__dirname, "demo.html");
  res.sendFile(filePath)
})

app.post('/localDevice_Location', (req,res) => {
  console.log(req.body);
  // axios.post(BE_URL + '/device_ip',req.body, req.headers)
  // .then(response => {
  //   console.log(response.data)
  //   return res.json(response.data);
  // })
  // .catch(error => {
  //   console.error(error)
  //   return res.send(error);
  // })
})

app.post('/uploadStreamdata', (req, res) => {
  console.log('uploadstram', req.body)
    axios.post(BE_URL + '/uploadStreamdata', req.body)
    .then(response => {
      console.log('uploadstreamData' , response.data)
      res.send(response.data);
    })
    .catch(error => {
      console.error(error)
    })
});

app.post('/uploadStreamdataVideo', (req, res) => {
  const storage = multer.diskStorage({
    filename: (req, file, cb) => {
      const fileExt = file.originalname.split(".").pop();
      const filename = `${new Date().getTime()}.${fileExt}`;
      cb(null, filename);
    },
  });

  const fileFilter = (req, file, cb) => {
    if (file.mimetype === "video/mp4") {
      cb(null, true);
    } else {
      cb(
        {
          message: "Unsupported File Format",
        },
        false
      );
    }
  };

  const upload = multer({
    storage,
    limits: {
      fieldNameSize: 200,
      fileSize: 5 * 1024 * 1024,
    },
    fileFilter,
  }).single("file");

  upload(req, res, (err) => {
    if (err) {
      return res.send(err);
    }
    if (req.fileValidationError) {
        return res.send(req.fileValidationError);
    }
    else if (!req.file) {
        return res.send('Please select video to upload');
    }
    else if (err instanceof multer.MulterError) {
        return res.send(err);
    }
    else if (err) {
        return res.send(err);
    }
    console.log(req.file)
    axios.post(BE_URL + '/uploadStreamdata',req.file)
    .then(response => {
      console.log('uploaddata', response.data)
      res.send(response.data);
    })
    .catch(error => {
      console.error(error)
    })
  });
});

app.get('/test', (req,res) => {
  axios.get(BE_URL + '/test', req.headers)
  .then(response => {
    console.log(response.data)
    return res.json(response.data);
  })
  .catch(error => {
    console.error(error)
    return res.send(error);
  })
})


app.get('/notify', (req, res) => {
  console.log('coming to get notify')
  io.sockets.emit('noted');
  res.send("Notify Successfully.");
})

app.get('/getCoords', (req , res) => {
  console.log('getCoords')
  io.sockets.emit('notice');
  res.send(latLong)
  console.log('getCroodslatlong', latLong)
}) 

let saveSoS = (data) => {
  console.log('beforeSavesos' , data)
  axios.post(BE_URL + '/saveSOS', data)
  .then(response => {
    console.log('saveSOS', response.data)
  })
  .catch(error => {
    console.log('savesoserr' , error)
    console.error(error)
  })
}

app.get('/getSOS', (req,res) => {
  console.log('getSOSreq', req)
  axios.get(BE_URL + '/getSOS', req.headers)
  .then(response => {
    console.log('getSOS', response.data)
    return res.json(response.data);
  })
  .catch(error => {
    console.error(error)
    console.log('err' , error)
    return res.send(error);
  })
})


app.post('/broadcast', (req, res) => {
  console.log('broadcast', req.body)
  saveSoS(req.body)
  io.sockets.emit('broadcast', req.body)
  res.send('successfully broadcast')
})

app.post('/locationDetails', (req, res) => {
  console.log(req.body)
  saveSoS(req.body)
  io.sockets.emit('broadcast', req.body)
  res.send('successfully broadcast')
})

const server = https.createServer(options, app).listen(port, '0.0.0.0', () => {
  console.log(`Server is running on https://localhost:${port}`);
});
// const server = app.listen(port, '0.0.0.0', () => {
//   console.log(`Server is running on http://localhost:${port}`)
// })

const io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
});

io.on("connection", function (socket) {
  console.log("Made socket connection");
  socket.on("notification", function (data) {
    console.log('client to proxy:', JSON.stringify(data))
    latLong = data
    console.log ('latlong',latLong)
  });
});


