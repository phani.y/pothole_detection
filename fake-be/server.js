
const express = require('express');
const bodyParser = require('body-parser');
const potholedata = require('./pothole.json') 
const userdata = require('./user.json')
const cors = require('cors');
const app = express();
app.use(cors());
const path = require('path');
const socket = require("socket.io");
var multer  = require('multer')
const env=process.env
const port = 5000;
const helpers = require('./helpers');
const https = require('https');
const fs = require('fs');

const privateKey = fs.readFileSync(path.join(__dirname, 'certs', 'tls.key'));
const certificate = fs.readFileSync(path.join(__dirname, 'certs', 'tls.crt'));
const options = {
   key: privateKey,
   cert: certificate
};

app.use('/static', express.static(path.join(__dirname, './public')));
app.use(bodyParser.json());

app.get('/', function(req, res) {
  res.send('connected to server successfully');
})

app.get('/live', (req , res) => {
  res.send('http://resource.wangdaodao.com/test.flv')
})
app.get('/pothole_data', (req , res) => {
  // console.log(potholedata)
  res.send(potholedata)
})

app.get('/users_data', (req , res) => {
  // console.log(potholedata)
  res.send(userdata)
})

app.get('/test', (req , res) => {
  console.log('tested succesfully!!!')
  res.send('tested succesfully!!!')
})
app.get('/getSOS', (req , res) => {
  let data = [
    [
      "23.0225",
      "72.5714",
      "sos",
      "4.4.4.4"
    ]
  ]
  res.send(data)
})
app.get('/videoFile', (req , res) => {
  res.send('https://unsplash.com/photos/Ctw-NV4UeVc')
})
app.post('/device_ip', (req , res) => {
  console.log('reqqqq', req.body)
  res.send('save data successfully!!!')
})
app.get('/location_map', (req , res) => {
  res.send('get location map')
})
app.get('/update_location', (req , res) => {
  res.send('update location')
})

app.get('/showLive/:id', function(req, res) {
  res.send('https://im.indiatimes.in/media/content/2015/Jul/cow_1436255044.gif');    
});


app.post('/uploadStreamdata', (req, res) => {
  console.log(req.file)
  const storage = multer.diskStorage({
    filename: (req, file, cb) => {
      const fileExt = file.originalname.split(".").pop();
      const filename = `${new Date().getTime()}.${fileExt}`;
      cb(null, filename);
    },
  });

  const fileFilter = (req, file, cb) => {
    if (file.mimetype === "video/mp4") {
      cb(null, true);
    } else {
      cb(
        {
          message: "Unsupported File Format",
        },
        false
      );
    }
  };

  const upload = multer({
    storage,
    limits: {
      fieldNameSize: 200,
      fileSize: 5 * 1024 * 1024,
    },
    fileFilter,
  }).single("file");

  upload(req, res, (err) => {
    if (err) {
      return res.send(err);
    }
    if (req.fileValidationError) {
        return res.send(req.fileValidationError);
    }
    else if (!req.file) {
        return res.send('Please select video to upload');
    }
    else if (err instanceof multer.MulterError) {
        return res.send(err);
    }
    else if (err) {
        return res.send(err);
    }
    res.send(`upload successfully.`);
  });
});

app.get('/notify', (req, res) => {
  console.log('coming to get notify')
  io.sockets.emit('notice');
  res.send("Notify Successfully.");
})



const server = https.createServer(options, app).listen(port, '0.0.0.0', () => {
  console.log(`Server is running on https://localhost:${port}`);
});
